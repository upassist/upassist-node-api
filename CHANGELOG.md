# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [8.0.0](https://bitbucket.org/upassist/upassist-node-api/compare/4.0.1...8.0.0) (2022-10-17)
